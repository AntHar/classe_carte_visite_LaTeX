%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Ce fichier est le template et la classe de la carte de visite. Par défaut il 
% ne faut pas le modifier. Pour une utilisation « classique » il suffit d'éditer 
% le fichier .tex.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\ProvidesClass{carte_visite}
\LoadClass[oneside,12pt]{article}

% Polices

\usepackage{xltxtra}
\defaultfontfeatures{Ligatures=TeX,Scale=MatchLowercase}
\setmainfont{Linux Libertine O}
\setsansfont{Linux Biolinum O}

% Liste des packages utilisés

\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{amssymb}
\usepackage{makeidx}
\usepackage{wallpaper}
\usepackage{graphicx}
\usepackage{pifont}
\usepackage[colorlinks=true,linkcolor=blue,urlcolor=black,citecolor=black]{hyperref}
\usepackage{array}
% Format de la carte avec définition des marges.
\usepackage[papersize={85mm,55mm},top=5mm, bottom=5mm, left=5mm , right=5mm]{geometry}
\thispagestyle{empty}
\setlength{\parindent}{0cm}

% Création des différentes macros utilisées dans le .tex.
% \nom
\newcommand*{\nom}[2]{\def\@nom{\large\textbf{#1 \textsc{#2}}}}
% \statut
\newcommand*{\statut}[1]{\def\@statut{\Large \textsc{#1}}}
% \entreprise
\newcommand*{\entreprise}[1]{\def\@entreprise{\normalsize \textbf{#1}}}
% \email
\newcommand*{\email}[1]{\def\@email{\texttt{\href{mailto:#1}{#1}}}}
% \site
\newcommand*{\site}[1]{\def\@site{\texttt{\href{http://#1}{#1}}}}
% \tel
\newcommand*{\tel}[5]{\def\@tel{\textit{#1\,#2\,#3\,#4\,#5}}}
%\pro
\newcommand*{\pro}[5]{\def\@pro{\textit{#1\,#2\,#3\,#4\,#5}}}
% \mobile
\newcommand*{\mobile}[5]{\def\@mobile{\textit{#1\,#2\,#3\,#4\,#5}}}
% \logo
\newcommand*{\logo}[1]{\def\@logo{#1}}
% \adresse
\newcommand*{\adresse}[2]{\def\@adresse{#1, #2}}
% Ville
\newcommand*{\ville}[2]{\def\@ville{#1 #2}}
% \pgp
\newcommand*{\pgp}[1]{\def\@pgp{#1}}
\newcommand*{\id}[1]{\def\@id{\href{http://pgp.mit.edu/pks/lookup?op=vindex&search=#1}{\texttt{#1}}}}
\newcommand*{\fingerprint}[1]{\def\@fingerprint{\texttt{#1}}}

% Mise en page de la carte.
\newcommand*{\printcarte}{
\begin{flushright}
 {\sffamily \@statut}\\ \vspace{2mm}
 {\sffamily \@nom} \\
 {\sffamily \@entreprise} \\\vspace{2mm}
\end{flushright}
\begin{flushright}
\begin{minipage}[l]{50mm}
\begin{scriptsize}
\begin{tabbing}
\hspace{5mm}\=\kill
\Pisymbol{mvs}{'153} \> \@email \\ 
\ding{'51} \> \@adresse \\ 
\Pisymbol{mvs}{'155} \> \@ville \\
\Pisymbol{mvs}{'315} \> \@site
\ifthenelse{\isundefined{\@tel}}{}{\\ \ding{'45} \> \@tel}
\ifthenelse{\isundefined{\@pro}}{}{\\ \Pisymbol{mvs}{'111} \> \@pro}
\ifthenelse{\isundefined{\@mobile}}{}{\\ \Pisymbol{mvs}{'110} \> \@mobile}
\end{tabbing} 
\end{scriptsize}
\end{minipage}\qquad
\LLCornerWallPaper{0.5}{\@logo}
\end{flushright}

\ifthenelse{\@pgp=0}{}{\newpage \ClearWallPaper \centering {\sffamily Clé publique PGP :} \\ \@id \\\bigskip {\sffamily Empreinte de la clé :} \\ \@fingerprint}
}

% Fin de la classe.