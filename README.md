Cette classe permet de créer une carte de visite au format 85 mm par 55 mm avec LaTeX.

Le format est plutôt « simple » graphiquement, c’est une volonté de ma part : en effet,
je ne peux pas faire tous les cas de figure et je ne peux pas m’adapter à toutes les envies.
C’est donc une carte « basique » mais efficace regroupant toutes les informations
importantes que j’ai souhaité créer.
Libre à vous ensuite de l’adapter selon vos envies. :-)

Comme toujours, le code est composé de deux documents principaux :
* le fichier .cls correspondant à la classe qui va faire la mise en page (il n’est pas à toucher
sauf si vous voulez le modifier ou l’adapter) ;
* le fichier .tex qui va demander les différents critères qui vont compléter le document. Celui-ci
est à compléter. Il est plutôt bien commenté. :-)

Les deux fichiers ne doivent pas être séparés.

Pour compiler le fichier : 
$ xelatex carte.tex